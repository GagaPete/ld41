extends "res://card.gd"

const SUPPORT_SCENE = preload("res://support.tscn")

func use():
	var support = SUPPORT_SCENE.instance()
	$"/root/game/layers/ground/tanks".add_child(support)
	support.player = player
	queue_free()