extends "res://card.gd"

const BOMB_SCENE = preload("res://bomb.tscn")

var active = false

func use():
	if not active:
		player_node.connect("choose_bombing_position", self, "_on_choose_bombing_position")
		active = true

func _on_choose_bombing_position(position):
	print("Boom")
	var target = $"/root/game/layers/ground/explosions"
	var bomb = BOMB_SCENE.instance()
	target.add_child(bomb)
	bomb.position = Vector2(position.x, -100)
	queue_free()