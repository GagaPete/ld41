extends "res://card.gd"

const TANK_SCENE = preload("res://tank.tscn")

func use():
	var tank = TANK_SCENE.instance()
	$"/root/game/layers/ground/tanks".add_child(tank)
	tank.player = player
	queue_free()