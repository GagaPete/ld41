extends Camera2D
const SPEED = 1600
const RANGE = 400

func _physics_process(delta):
	if get_local_mouse_position().y > (1050 - 128):
		return
	
	var mouse_pos = get_local_mouse_position().x
	if mouse_pos <= RANGE:
		position.x -= SPEED * delta * (1 - cos(PI * 0.5 * (RANGE - mouse_pos) / RANGE))
	if mouse_pos >= 1920 - RANGE:
		position.x += SPEED * delta * (1 - cos(PI * 0.5 * (RANGE - (1920 - mouse_pos)) / RANGE))
	position.x = clamp(position.x, 0, 4080)