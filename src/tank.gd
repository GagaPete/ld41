extends PathFollow2D

const EXPLOSION_SCENE = preload("res://effects/explosion.tscn")
const BULLET_SCENE = preload("res://bullet.tscn")
const Common = preload("res://common.gd")
const MIN_SPEED = 170
const MAX_SPEED = 190

export(Common.Player) var player setget set_player

onready var node = $node
onready var enemies_in_shooting_range = []
onready var speed = rand_range(MIN_SPEED, MAX_SPEED)

var health = 100
var direction

func _process(delta):
	if enemies_in_shooting_range.size() > 0:
		if $animation.current_animation != "shoot":
			$animation.play("shoot")
	elif enemies_in_shooting_range.size() == 0:
		offset += speed * delta * direction
		if $animation.current_animation != "drive":
			$animation.play("drive")
	
	node.scale = Vector2(direction, 1) * 0.3
	$health.value = health
	if health <= 0.0:
		replace_by_explosion()

func set_player(value):
	if player != null:
		remove_from_group("player_%d"%player)
	player = value
	if player != null:
		add_to_group("player_%d"%player)
	if player == Common.Player_2:
		direction = -1
	else:
		direction = 1

func _on_shooting_range_area_entered(area):
	if area.is_in_group("target") and area.get_parent().player != player:
		enemies_in_shooting_range.append(area.get_parent())

func _on_shooting_range_area_exited(area):
	enemies_in_shooting_range.erase(area.get_parent())

func shoot():
	var bullet = BULLET_SCENE.instance()
	bullet.player = player
	$"/root/game/layers/ground/bullets".add_child(bullet)
	bullet.global_rotation = self.global_rotation
	if direction == -1:
		bullet.global_rotation += PI
	bullet.global_position = $node/bullet_spawn.global_position

func replace_by_explosion():
	var target = $"/root/game/layers/ground/explosions"
	var explosion = EXPLOSION_SCENE.instance()
	target.add_child(explosion)
	explosion.scale = Vector2(0.5, 0.5)
	explosion.emitting = true
	explosion.global_position = global_position
	queue_free()