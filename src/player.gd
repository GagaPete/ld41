extends Node2D

signal ready
signal loose
signal choose_bombing_position(position)

const Common = preload("res://common.gd")
const PROGRESS_SPEED = .75

export(NodePath) var deck_path setget set_deck_path
export(NodePath) var hand_path setget set_hand_path
export(Common.Player) var player

onready var hand
onready var deck

var progress_to_next_draw = 0.0
var hand_initialised = false

func _process(delta):
	if deck == null:
		deck = get_node(deck_path)
		if deck == null:
			return
	
	if hand == null:
		hand = get_node(hand_path)
		if hand == null:
			return

	for card in deck.get_children():
		card.player = player
		card.player_node = self
	
	if not hand_initialised:
		for i in range(0, 5):
			draw_card()
		hand_initialised = true
	
	if hand.get_child_count() == 5:
		progress_to_next_draw = 0.0
	else:
		progress_to_next_draw += PROGRESS_SPEED * delta
		# TODO: Bonus for captured stuff
		if progress_to_next_draw >= 1.0:
			draw_card()
			progress_to_next_draw -= 1.0
	
	if hand.get_child_count() == 0 and deck.get_child_count() == 0 and player != null and get_tree().get_nodes_in_group("player_%d"%player).size() == 0:
		emit_signal("loose")
		print("loose")

func _on_lose_zone_area_entered(area):
	print("Enter")
	if player != null and area.is_in_group("target") and not area.get_parent().is_in_group("player_%d"%player):
		emit_signal("loose")
		print("loose")
	
func get_enemy_targets():
	var group
	if player == Common.Player_2:
		group = "player_%d"%Common.Player_1
	else:
		group = "player_%d"%Common.Player_2
	return get_tree().get_nodes_in_group(group)

func draw_card():
	if hand == null or deck.get_child_count() == 0:
		return

	var children = deck.get_children()
	var draw = children[rand_range(0, children.size())]
	deck.remove_child(draw)
	hand.add_child(draw)

func set_deck_path(value):
	deck_path = value
	deck = null

func set_hand_path(value):
	hand_path = value
	hand = null