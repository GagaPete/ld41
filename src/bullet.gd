extends Area2D

const SPEED = 800

var player

func _process(delta):
	position = position + Vector2(SPEED, 0).rotated(global_rotation) * delta

func _on_bullet_area_entered(area):
	if area.is_in_group("target") and area.get_parent().player != player:
		area.get_parent().health -= 15
		queue_free()