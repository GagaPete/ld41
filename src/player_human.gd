extends "res://player.gd"

func _process(delta):
	._process(delta)
	
	for card in hand.get_children():
		if not card.is_connected("pressed", card, "use"):
			card.connect("pressed", card, "use")

func _unhandled_input(event):
	if event is InputEventMouseButton:
		if event.pressed and event.button_index == BUTTON_LEFT:
			emit_signal("choose_bombing_position", event.global_position + $"/root/game/camera".global_position)