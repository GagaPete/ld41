extends Particles2D

const SMOKE_SCENE = preload("res://effects/smoke.tscn")

func _ready():
	emitting = true

func _on_timer_timeout():
	var target = $".."
	var smoke = SMOKE_SCENE.instance()
	smoke.preprocess = 0
	target.add_child(smoke)
	smoke.emitting = true
	smoke.global_position = global_position
	queue_free()