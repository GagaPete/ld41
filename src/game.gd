extends Node

func _ready():
	randomize()
	get_tree().paused = true
	if OS.has_feature("web"):
		OS.set_window_maximized(true)

func _on_deckbuilder_filled():
	$intro_camera/animation.play("deckbuilder_to_game")

func start_game():
	get_tree().paused = false
	$camera.current = true

func reset_game():
	get_tree().reload_current_scene()

func _on_player_human_loose():
	get_tree().paused = true
	$intro_camera/animation.play("game_to_defeat")

func _on_player_ai_loose():
	get_tree().paused = true
	$intro_camera/animation.play("game_to_victory")