extends CenterContainer

signal filled

const TANK_CARD_SCENE = preload("res://cards/tank_card.tscn")
const BOMB_CARD_SCENE = preload("res://cards/bomb_card.tscn")
const SUPPORT_CARD_SCENE = preload("res://cards/support_card.tscn")

onready var grid = $vbox/slots/grid
onready var tank = $vbox/pool/tank_card
onready var bomb = $vbox/pool/bomb_card
onready var support = $vbox/pool/support_card

func add_card(scene):
	if grid.get_child_count() < 12:
		var card = scene.instance()
		$vbox/slots/grid.add_child(card)
		card.connect("pressed", $vbox/slots/grid, "remove_child", [card], CONNECT_ONESHOT)
		reorder()
		if grid.get_child_count() == 12:
			emit_signal("filled")

func reorder():
	var cards = grid.get_children()
	var tank_cards = []
	var bomb_cards = []
	var support_cards = []
	for card in cards:
		if card.is_in_group("tank_card"):
			tank_cards.append(card)
		elif card.is_in_group("bomb_card"):
			bomb_cards.append(card)
		else:
			support_cards.append(card)
	var i = 0
	for card in tank_cards:
		grid.move_child(card, i)
		i += 1
	for card in bomb_cards:
		grid.move_child(card, i)
		i += 1
	for card in support_cards:
		grid.move_child(card, i)
		i += 1

func _on_tank_card_pressed():
	add_card(TANK_CARD_SCENE)

func _on_bomb_card_pressed():
	add_card(BOMB_CARD_SCENE)

func _on_support_card_pressed():
	add_card(SUPPORT_CARD_SCENE)