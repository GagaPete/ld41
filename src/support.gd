extends PathFollow2D

const EXPLOSION_SCENE = preload("res://effects/explosion.tscn")
const Common = preload("res://common.gd")

export(Common.Player) var player setget set_player

onready var node = $node
onready var nearby_tanks = []
onready var front_tanks = []

const CATCHUP_SPEED = 220
const FOLLOW_SPEED = 170

var health = 80
var direction = 0

func _process(delta):
	$animation.playback_speed = 1
	
	var speed = CATCHUP_SPEED
	if front_tanks.size() > 0:
		speed = FOLLOW_SPEED
	for target in front_tanks:
		if target.is_in_group("tank") and target.enemies_in_shooting_range.size() > 0:
			speed = 0
			$animation.playback_speed = 0
			break
	offset += speed * delta * direction
	
	node.scale = Vector2(direction, 1) * 0.3
	$health.value = health
	if health <= 0.0:
		replace_by_explosion()

func _on_range_area_entered(area):
	if player != null and area.is_in_group("tank"):
		if area.get_parent().is_in_group("player_%d"%player):
			nearby_tanks.append(area.get_parent())

func _on_range_area_exited(area):
	nearby_tanks.erase(area.get_parent())

func _on_front_area_entered(area):
	if player != null and area.is_in_group("target"):
		if area.get_parent().is_in_group("player_%d"%player):
			front_tanks.append(area.get_parent())

func _on_front_area_exited(area):
	front_tanks.erase(area.get_parent())

func set_player(value):
	if player != null:
		remove_from_group("player_%d"%player)
	player = value
	if player != null:
		add_to_group("player_%d"%player)
	if player == Common.Player_2:
		direction = -1
	else:
		direction = 1

func replace_by_explosion():
	var target = $"/root/game/layers/ground/explosions"
	var explosion = EXPLOSION_SCENE.instance()
	target.add_child(explosion)
	explosion.scale = Vector2(0.5, 0.5)
	explosion.emitting = true
	explosion.global_position = global_position
	queue_free()

func _on_timer_timeout():
	for tank in nearby_tanks:
		tank.health = min(100, tank.health + 1)