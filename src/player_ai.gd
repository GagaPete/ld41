extends "res://player.gd"

var is_bombing = false

func _on_action_timer_timeout():
	var cards = hand.get_children()
	if cards.size() > 0:
		var card = cards[rand_range(0, cards.size())]
		if card.is_in_group("bomb_card"):
			if not is_bombing:
				is_bombing = true
				card.use()
				return
		else:
			card.use()
	
	if is_bombing:
		var enemies = get_enemy_targets()
		if enemies.size() > 0:
			emit_signal("choose_bombing_position", enemies[rand_range(0, enemies.size())].global_position)
			is_bombing = false