extends Area2D

const EXPLOSION_SCENE = preload("res://effects/explosion.tscn")
const SPEED = 750

onready var areas_in_radius = []

func _process(delta):
	position.y += SPEED * delta

func _on_bomb_body_entered(body):
	var target = $".."
	var explosion = EXPLOSION_SCENE.instance()
	target.add_child(explosion)
	explosion.emitting = true
	explosion.global_position = global_position
	for area in areas_in_radius:
		area.get_parent().health -= 45
	queue_free()

func _on_radius_area_entered(area):
	if area.is_in_group("target"):
		areas_in_radius.append(area)

func _on_radius_area_exited(area):
	areas_in_radius.erase(area)